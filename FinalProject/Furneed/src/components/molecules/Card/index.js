import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
// import {TouchableOpacity} from 'react-native-gesture-handler';
import {BoxShadow} from 'react-native-shadow';
import {
  IconFavoriteOff,
  IconFavoriteOn,
  IconStarOn,
  IconRemove,
} from '../../../assets';
import {Gap} from '../../index';

const Card = ({
  id,
  image,
  name,
  desc,
  qty,
  price,
  status,
  isFavorite,
  rating,
  onPress,
  productFor,
}) => {
  const windowWidth = Dimensions.get('window').width;
  const boxWidth = windowWidth - 60;
  const shadowOpt = {
    width: boxWidth,
    height: 70,
    color: '#000',
    border: 2,
    radius: 5,
    opacity: 0.1,
    x: 1,
    y: 2,
    style: {marginVertical: 5},
  };

  const formatRupiah = (bilangan) => {
    var number_string = bilangan.toString();
    var sisa = number_string.length % 3;
    var rupiah = number_string.substr(0, sisa);
    var ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
      var separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    return rupiah;
  };
  const listProduct = () => {
    switch (productFor) {
      case 'FOR-CART':
        return (
          <BoxShadow setting={shadowOpt}>
            <View style={styles.containerCart}>
              <View style={styles.imageContainer}>
                <Image style={styles.imageCart} source={image} />
              </View>
              <View style={styles.productContainer}>
                <Text style={styles.name}>{name}</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={styles.counterContainer}>
                    <TouchableOpacity>
                      <View style={styles.minus}>
                        <Text style={{fontSize: 10}}>-</Text>
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.counter}>{qty}</Text>
                    <TouchableOpacity>
                      <View style={styles.plus}>
                        <Text style={{fontSize: 10}}>+</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <Text style={styles.price}>{price}</Text>
                </View>
              </View>
            </View>
          </BoxShadow>
        );
      case 'FOR-ORDER-PROGRESS':
        return (
          <View style={{marginBottom: 5}}>
            <View style={styles.containerOrder}>
              <View style={styles.imageContainer}>
                <Image style={styles.imageOrder} source={image} />
              </View>
              <View style={{justifyContent: 'center', flex: 1}}>
                <Text style={styles.name}>{name}</Text>
                <Gap height={5} />
                <Text style={styles.price}>{price}</Text>
              </View>
              <View style={styles.actionOrder}>
                <Text></Text>
                <Text style={styles.status(status)}>{status}</Text>
              </View>
            </View>
          </View>
        );
      case 'FOR-ORDER-HISTORY':
        return (
          <View style={{marginBottom: 5}}>
            <View style={styles.containerOrder}>
              <View style={styles.imageContainer}>
                <Image style={styles.imageOrder} source={image} />
              </View>
              <View style={{justifyContent: 'center', flex: 1}}>
                <Text style={styles.name}>{name}</Text>
                <Gap height={5} />
                {/* <Text style={styles.qty}>x{qty}</Text> */}
                <Text style={styles.price}>{price}</Text>
              </View>
              <View style={styles.actionOrder}>
                <Text></Text>
                <Text
                  style={{
                    fontFamily: 'Poppins-Regular',
                    fontSize: 10,
                    color: '#C4C4C4',
                  }}>
                  Jan 08, 2021
                </Text>
              </View>
            </View>
          </View>
        );

      default:
        return (
          <View style={styles.container}>
            <View style={styles.imageContainer}>
              <Image
                style={styles.image}
                source={{
                  uri: `https://api-furneed.herokuapp.com/images/${image}`,
                }}
              />
            </View>
            <View style={styles.productContainer}>
              <Text style={styles.name}>{name}</Text>
              <View style={styles.descContainer(windowWidth / 2.2)}>
                <Text style={styles.desc}>{desc}</Text>
              </View>
              <Text style={styles.price}>Rp{formatRupiah(price)}</Text>
            </View>
            <View style={styles.action}>
              <TouchableOpacity onPress={onPress}>
                <View style={styles.favoriteContainer}>
                  {isFavorite ? (
                    <IconFavoriteOn width={15} height={15} />
                  ) : (
                    <IconFavoriteOff width={15} height={15} />
                  )}
                </View>
              </TouchableOpacity>
              <View style={styles.rating}>
                <IconStarOn width={13} height={13} />
                <Text style={styles.ratingNumber}>{rating}</Text>
              </View>
            </View>
          </View>
        );
    }
  };

  return listProduct();
};

export default Card;

const styles = StyleSheet.create({
  container: {
    borderRadius: 5,
    minHeight: 90,
    maxHeight: 90,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 10,
  },
  containerCart: {
    borderRadius: 5,
    minHeight: 70,
    maxHeight: 70,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 10,
  },
  containerOrder: {
    minHeight: 60,
    maxHeight: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 10,
  },
  imageContainer: {
    // borderWidth: 1,
    // borderColor: 'red',
    marginRight: 20,
  },
  // image: {width: 70, height: 70, resizeMode: 'cover'},
  image: {width: 70, height: 70, resizeMode: 'cover'},
  imageOrder: {width: 40, height: 40, resizeMode: 'cover'},
  imageCart: {width: 50, height: 50, resizeMode: 'cover'},
  productContainer: {
    justifyContent: 'space-between',
    flex: 1,
    marginRight: 10,
  },

  name: {
    fontFamily: 'Poppins-Medium',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#020202',
  },
  counterContainer: {
    // borderWidth: 1,
    borderRadius: 3,
    borderColor: '#C4C4C4',
    width: 60,
    height: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  minus: {
    backgroundColor: '#ebebeb',
    width: 20,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  counter: {
    fontFamily: 'Poppins-Regular',
    fontSize: 8,
    alignSelf: 'center',
    paddingTop: 3,
  },
  plus: {
    backgroundColor: '#12A89D',
    width: 20,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  qty: {
    marginTop: 5,
    fontFamily: 'Poppins-Medium',
    fontSize: 10,
    color: '#C4C4C4',
  },
  descContainer: (width) => ({
    // borderWidth: 1,
    // borderColor: 'blue',
    minHeight: 35,
    maxHeight: 35,
    // maxWidth: 135,
  }),
  desc: {
    fontFamily: 'Poppins-Medium',
    fontSize: 10,
    color: '#020202',
    textAlign: 'justify',
  },
  status: (status) => ({
    fontFamily: 'Poppins-Medium',
    fontSize: 10,
    color: status == 'Process' ? '#12A89D' : 'red',
    textAlign: 'justify',
  }),
  price: {
    fontFamily: 'Poppins-Medium',
    fontSize: 10,
    color: '#FCA500',
    fontWeight: 'bold',
  },
  action: {justifyContent: 'space-between', alignItems: 'center'},
  actionOrder: {justifyContent: 'space-between'},
  favoriteContainer: {paddingHorizontal: 10, paddingBottom: 10},
  rating: {flexDirection: 'row', alignItems: 'center'},
  ratingNumber: {fontFamily: 'Poppins-regular', fontSize: 10, marginLeft: 3},
});
