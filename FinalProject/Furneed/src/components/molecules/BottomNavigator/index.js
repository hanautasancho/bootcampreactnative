import React, {Fragment} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {
  IconHomeOn,
  IconHomeOff,
  IconTimeOn,
  IconTimeOff,
  IconCartOn,
  IconCartOff,
  IconProfileOn,
  IconProfileOff,
} from '../../../assets';

const resize = {width: 20, height: 20};

const Icon = ({label, focus}) => {
  switch (label) {
    case 'Home':
      return (
        <View style={styles.menuContainer}>
          {focus ? (
            <Fragment>
              <IconHomeOn {...resize} />
              <Text style={styles.textOn}>Home</Text>
            </Fragment>
          ) : (
            <Fragment>
              <IconHomeOff {...resize} />
              <Text style={styles.textOff}>Home</Text>
            </Fragment>
          )}
        </View>
      );
    case 'Order':
      return (
        <View style={styles.menuContainer}>
          {focus ? (
            <Fragment>
              <IconTimeOn {...resize} />
              <Text style={styles.textOn}>Order</Text>
            </Fragment>
          ) : (
            <Fragment>
              <IconTimeOff {...resize} />
              <Text style={styles.textOff}>Order</Text>
            </Fragment>
          )}
        </View>
      );
    case 'Cart':
      return (
        <View style={styles.menuContainer}>
          {focus ? (
            <Fragment>
              <IconCartOn {...resize} />
              <Text style={styles.textOn}>Cart</Text>
            </Fragment>
          ) : (
            <Fragment>
              <IconCartOff {...resize} />
              <Text style={styles.textOff}>Cart</Text>
            </Fragment>
          )}
        </View>
      );
    case 'Profile':
      return (
        <View style={styles.menuContainer}>
          {focus ? (
            <Fragment>
              <IconProfileOn {...resize} />
              <Text style={styles.textOn}>Profile</Text>
            </Fragment>
          ) : (
            <Fragment>
              <IconProfileOff {...resize} />
              <Text style={styles.textOff}>Profile</Text>
            </Fragment>
          )}
        </View>
      );
    default:
      return <IconHomeOff />;
  }
};

const BottomNavigator = ({state, descriptors, navigation}) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            activeOpacity={0.3}>
            <Icon label={label} focus={isFocused} />
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default BottomNavigator;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingTop: 10,
    paddingBottom: 5,
    paddingHorizontal: 40,
    justifyContent: 'space-between',
    borderTopWidth: 0.3,
    borderColor: '#BFBFBF',
  },
  menuContainer: {alignItems: 'center'},
  textOn: {fontFamily: 'Poppins-Medium', fontSize: 8, color: '#FCA500'},
  textOff: {fontFamily: 'Poppins-Medium', fontSize: 8, color: '#C4C4C4'},
});
