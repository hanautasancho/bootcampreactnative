import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {IconBack} from '../../../assets';

const Header = ({title, onBack, hasDivider}) => {
  return (
    <View style={styles.container(hasDivider)}>
      {onBack && (
        <View style={styles.containerBack}>
          <TouchableOpacity onPress={() => onBack()}>
            <IconBack width={30} height={30} />
          </TouchableOpacity>
        </View>
      )}

      <Text style={styles.title}>{title}</Text>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: (hasDivider) => ({
    backgroundColor: 'white',
    paddingHorizontal: 30,
    paddingVertical: 20,
    borderWidth: hasDivider ? 0.5 : 0,
    borderColor: '#C4C4C4',
    marginBottom: 30,
  }),
  containerBack: {
    paddingHorizontal: 10,
    position: 'absolute',
    marginTop: 16,
    marginLeft: 15,
  },
  back: {
    fontSize: 16,
    // backgroundColor: 'blue',
    paddingHorizontal: 10,
    position: 'absolute',
    marginTop: -4,
    marginLeft: -15,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    fontWeight: 'bold',
    color: '#020202',
    alignSelf: 'center',
  },
});
