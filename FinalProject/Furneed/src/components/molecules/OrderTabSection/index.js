import React, {Fragment} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  TextInput as TextInputRN,
} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {Card, Gap} from '../../index';
import {IconSearch} from '../../../assets';

import ImageProduct1 from '../../../assets/Dummy/lidkullen.jpg';
import ImageProduct2 from '../../../assets/Dummy/rakkestad.jpg';
import ImageProduct3 from '../../../assets/Dummy/skarig.jpg';
import ImageProduct4 from '../../../assets/Dummy/gronlid.jpg';

// const asd = TabBarTop.tabWidth;
const windowWidth = Dimensions.get('window').width;
const width = windowWidth / 2;
const indicatorWidth = width / 4;
const renderTabBar = (props) => (
  <TabBar
    {...props}
    indicatorStyle={{
      backgroundColor: '#FCA500',
      height: 3,
      width: indicatorWidth,
      alignSelf: 'center',
      marginLeft: '14.5%',
      // borderRadius: 10,
    }}
    style={{
      backgroundColor: 'white',
      elevation: 0,
      // shadowOpacity: 0,
      borderBottomColor: '#F2F2F2',
      borderBottomWidth: 1,
    }}
    tabStyle={{width: width}}
    renderLabel={({route, focused, color}) => (
      <Text
        style={{
          fontFamily: 'Poppins-Medium',
          color: focused ? '#020202' : '#8D92A3',
        }}>
        {route.title}
      </Text>
    )}
  />
);

const FirstRoute = () => (
  <Fragment>
    <View style={styles.searchContainer}>
      <View style={styles.searchSection}>
        <TouchableOpacity>
          <IconSearch />
        </TouchableOpacity>
        <TextInputRN
          style={styles.input}
          placeholder="Search"
          placeholderTextColor="#FCA500"
        />
      </View>
    </View>
    <Gap height={10} />

    <Card
      image={ImageProduct1}
      name="LIDKULLEN"
      qty={3}
      price="Rp122.000"
      status="Process"
      onPress={() => onRemoveClick()}
      productFor="FOR-ORDER-PROGRESS"
    />
    <Card
      image={ImageProduct3}
      name="SKÄRIG"
      qty={3}
      price="Rp699.000"
      status="Process"
      onPress={() => onRemoveClick()}
      productFor="FOR-ORDER-PROGRESS"
    />
    <Card
      image={ImageProduct2}
      name="RAKKESTAD"
      qty={3}
      price="Rp1.932.000"
      status="Process"
      onPress={() => onRemoveClick()}
      productFor="FOR-ORDER-PROGRESS"
    />
    <Card
      image={ImageProduct4}
      name="GRÖNLID"
      qty={3}
      price="Rp1.799.000"
      status="Cancelled"
      onPress={() => onRemoveClick()}
      productFor="FOR-ORDER-PROGRESS"
    />
  </Fragment>
);

const SecondRoute = () => (
  <Fragment>
    <View style={styles.searchContainer}>
      <View style={styles.searchSection}>
        <TouchableOpacity>
          <IconSearch />
        </TouchableOpacity>
        <TextInputRN
          style={styles.input}
          placeholder="Search"
          placeholderTextColor="#FCA500"
        />
      </View>
    </View>
    <Gap height={10} />
    <Card
      image={ImageProduct2}
      name="RAKKESTAD"
      qty={3}
      price="Rp1.799.000"
      onPress={() => onRemoveClick()}
      productFor="FOR-ORDER-HISTORY"
    />
    <Card
      image={ImageProduct4}
      name="GRÖNLID"
      qty={3}
      price="Rp1.799.000"
      onPress={() => onRemoveClick()}
      productFor="FOR-ORDER-HISTORY"
    />
  </Fragment>
);

const initialLayout = {width: Dimensions.get('window').width};

const OrderTabSection = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: '1', title: 'In Progress'},
    {key: '2', title: 'History'},
  ]);

  const renderScene = SceneMap({
    1: FirstRoute,
    2: SecondRoute,
  });

  return (
    <View style={styles.page}>
      <TabView
        renderTabBar={renderTabBar}
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
      />
    </View>
  );
};

export default OrderTabSection;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: '#ECECEC'},
  scene: {
    flex: 1,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
  },
  searchSection: {
    // backgroundColor: 'red',
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    backgroundColor: 'white',
  },
  input: {
    padding: 8,
    paddingLeft: 10,
    height: 36,
    flex: 1,
  },
});
