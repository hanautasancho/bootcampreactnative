import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const Button = ({text, color = '#E42648', textColor = '#FFFFFF', onPress}) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
      <View style={styles.container(color)}>
        <Text style={styles.text(textColor)}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: (color) => ({
    backgroundColor: color,
    paddingVertical: 10,
    borderRadius: 8,
  }),
  text: (textColor) => ({
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    fontWeight: 'bold',
    color: textColor,
    textAlign: 'center',
  }),
});
