import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Picker} from '@react-native-picker/picker';

const Select = () => {
  const [val, setVal] = useState('jne');
  return (
    <View>
      <Picker
        selectedValue={val}
        style={{
          height: 20,
          width: 100,
        }}
        onValueChange={(itemValue, itemIndex) => setVal(itemValue)}>
        <Picker.Item label="JNE" value="jne" />
        <Picker.Item label="Sicepat" value="sicepat" />
      </Picker>
    </View>
  );
};

export default Select;

const styles = StyleSheet.create({
  label: {fontSize: 14, fontFamily: 'Poppins-Regular', color: '#020202'},
  // input: {
  //   borderWidth: 1,
  //   borderColor: '#020202',
  //   borderRadius: 8,
  //   padding: 3,
  //   justifyContent: 'center',
  //   height: 45,
  // },
});
