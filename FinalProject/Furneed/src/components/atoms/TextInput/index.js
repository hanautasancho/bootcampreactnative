import React from 'react';
import {StyleSheet, Text, View, TextInput as TextInputRN} from 'react-native';
import {Gap} from '..';

const TextInput = ({label, placeholder, ...restProps}) => {
  return (
    <View>
      <TextInputRN
        style={styles.input}
        placeholder={placeholder}
        {...restProps}
      />
      <Text style={styles.label}>{label}</Text>
      <Gap height={60} />
    </View>
  );
};

export default TextInput;

const styles = StyleSheet.create({
  label: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
    backgroundColor: 'white',
    paddingHorizontal: 5,
    alignSelf: 'flex-start',
    marginLeft: 30,
    marginTop: -60,
  },
  input: {
    borderWidth: 1,
    borderColor: '#DFDFDF',
    borderRadius: 8,
    padding: 10,
    height: 50, // default 50
  },
});
