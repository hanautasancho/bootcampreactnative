import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Image, ImageBackground} from 'react-native';
import {profileBackground, IconEdit} from '../../assets';
import profilePic from '../../assets/Dummy/profilePic.jpeg';

const Profile = () => {
  return (
    <View style={styles.page}>
      <ImageBackground
        source={profileBackground}
        style={styles.imageBackground}>
        <View style={styles.borderPhoto}>
          <Image style={styles.image} source={profilePic} />
        </View>
        <View style={styles.fullNameContainer}>
          <Text style={styles.fullName}>Mas Dimas Satria Ananda</Text>
          <IconEdit width={12} height={12} />
        </View>

        <Text style={styles.location}>JAKARTA, INDONESIA</Text>
      </ImageBackground>
      <View style={styles.detailContainer}>
        <View style={styles.input}>
          <View style={styles.labelContainer}>
            <Text style={styles.label}>Email</Text>
            <IconEdit width={15} height={15} />
          </View>
          <Text style={styles.value}>xxxxxxxxxxxxxx@gmail.com</Text>
        </View>
        <View style={styles.input}>
          <View style={styles.labelContainer}>
            <Text style={styles.label}>Password</Text>
            <IconEdit width={15} height={15} />
          </View>
          <Text style={styles.value}>************</Text>
        </View>
        <View style={styles.input}>
          <View style={styles.labelContainer}>
            <Text style={styles.label}>Address</Text>
            <IconEdit width={15} height={15} />
          </View>
          <Text style={styles.value}>Jl.yang lurus</Text>
        </View>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: 'white'},
  imageBackground: {
    height: 300,
    alignItems: 'center',
    justifyContent: 'center',
  },
  borderPhoto: {
    borderWidth: 3,
    borderColor: 'white',
    borderRadius: 60,
    width: 120,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 50,
    width: 100,
    height: 100,
  },
  fullNameContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  fullName: {
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    color: '#020202',
  },
  location: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#020202',
  },
  detailContainer: {marginTop: 30, paddingHorizontal: 50},
  input: {
    borderBottomWidth: 1,
    borderColor: '#C4C4C4',
    paddingBottom: 10,
    marginBottom: 10,
  },
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {fontFamily: 'Poppins-Regular', fontSize: 13, color: '#FCA500'},
  value: {fontFamily: 'Poppins-Regular', fontSize: 13, color: '#020202'},
});
