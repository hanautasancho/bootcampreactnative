import SplashScreen from './SplashScreen';
import SignIn from './SignIn';
import SignUp from './SignUp';
import Home from './Home';
import Order from './Order';
import Cart from './Cart';
import Profile from './Profile';
import Detail from './Detail';

export {SplashScreen, SignIn, SignUp, Home, Order, Cart, Profile, Detail};
