import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput as TextInputRN,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  ImageBackground,
} from 'react-native';
import {Gap, TextInput, Button, Header, Card} from '../../components';
import {IconSearch, IconBellOff, IconChat, Wallet, Voucher} from '../../assets';
import {BoxShadow} from 'react-native-shadow';
import {useSelector} from 'react-redux';

import ImageProduct1 from '../../assets/Dummy/lidkullen.jpg';
import ImageProduct2 from '../../assets/Dummy/rakkestad.jpg';
import ImageProduct3 from '../../assets/Dummy/skarig.jpg';

const Home = ({navigation}) => {
  const windowWidth = Dimensions.get('window').width;
  const width = windowWidth - 120;

  const shadowOpt = {
    width: width,
    height: 50,
    color: '#BCBCBC',
    border: 7,
    radius: 7,
    opacity: 0.3,
    x: 40,
    y: -25,
  };

  const onDetailClick = (product) => {
    navigation.navigate('Detail', {
      product: {
        image: product.image,
        name: product.name,
        desc: product.desc,
        stock: product.stock,
        price: product.price,
        rating: product.rating,
      },
    });
  };
  const onFavoriteClick = () => {
    alert('Favorite');
  };

  // const API = useSelector((state) => state.globalReducer).then((res) =>
  //   console.log(res.data.product),
  // );

  const [product, setProduct] = useState([]);

  const API = useSelector((state) => state.globalReducer);
  useEffect(async () => {
    const asd = await API.then((res) => {
      setProduct(res.data.product);
    });
  }, []);

  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <View style={styles.page}>
        {/* <Header title="Home" /> */}
        <StatusBar translucent backgroundColor="transparent" />
        <ImageBackground
          source={require('../../assets/Dummy/hero.jpg')}
          style={{height: 250}}>
          <View style={styles.searchContainer}>
            <View style={styles.searchSection}>
              <TouchableOpacity>
                <IconSearch />
              </TouchableOpacity>
              <TextInputRN
                style={styles.input}
                placeholder="Search"
                placeholderTextColor="#FCA500"
              />
            </View>
            <IconBellOff style={styles.notification} />
            <IconChat />
          </View>
          {/* <IconBack style={styles.back} width={30} height={30} /> */}
        </ImageBackground>
        <View style={styles.container}>
          <BoxShadow setting={shadowOpt}>
            <View
              style={{
                borderRadius: 7,
                width: width,
                height: 50,
                marginLeft: 40,
                position: 'absolute',
                top: -25,
                backgroundColor: 'white',
                zIndex: 1,
              }}>
              <View style={styles.walletVoucherContainer}>
                <View style={styles.walletContainer}>
                  <View style={styles.walletImageContainer}>
                    <Image source={Wallet} style={styles.walletImage} />
                  </View>
                  <View style={styles.cashContainer}>
                    <Text style={styles.cash}>Rp56.000</Text>
                  </View>
                </View>
                <Text style={{color: '#BCBCBC'}}>|</Text>
                <View style={styles.walletContainer}>
                  <View style={styles.walletImageContainer}>
                    <Image source={Voucher} style={styles.walletImage} />
                  </View>
                  <View style={styles.cashContainer}>
                    <Text style={styles.cash}>13 vouchers</Text>
                  </View>
                </View>
              </View>
            </View>
          </BoxShadow>
          <View style={styles.cardContainer}>
            <Text style={styles.hotKeyword}>Hot Keywords</Text>
            <Gap height={10} />
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <View>
                <View style={styles.hotKeywordContainer}>
                  <Text
                    style={{
                      padding: 5,
                      paddingTop: 10,
                      borderWidth: 1,
                      borderColor: '#C4C4C4',
                      borderRadius: 5,
                      // backgroundColor: 'red',
                      alignSelf: 'flex-start',
                      fontFamily: 'Poppins-Regular',
                      fontSize: 10,
                      marginRight: 10,
                    }}>
                    meja makan keluarga
                  </Text>
                  <Text
                    style={{
                      padding: 5,
                      paddingTop: 10,
                      borderWidth: 1,
                      borderColor: '#C4C4C4',
                      borderRadius: 5,
                      // backgroundColor: 'red',
                      alignSelf: 'flex-start',
                      fontFamily: 'Poppins-Regular',
                      fontSize: 10,
                      marginRight: 10,
                    }}>
                    rak buku
                  </Text>
                  <Text
                    style={{
                      padding: 5,
                      paddingTop: 10,
                      borderWidth: 1,
                      borderColor: '#C4C4C4',
                      borderRadius: 5,
                      // backgroundColor: 'red',
                      alignSelf: 'flex-start',
                      fontFamily: 'Poppins-Regular',
                      fontSize: 10,
                      marginRight: 10,
                    }}>
                    kursi buat anak-anak
                  </Text>
                </View>
                <View style={styles.hotKeywordContainer}>
                  <Text
                    style={{
                      padding: 5,
                      paddingTop: 10,
                      borderWidth: 1,
                      borderColor: '#C4C4C4',
                      borderRadius: 5,
                      // backgroundColor: 'red',
                      alignSelf: 'flex-start',
                      fontFamily: 'Poppins-Regular',
                      fontSize: 10,
                      marginRight: 10,
                    }}>
                    sofa minimalis
                  </Text>
                  <Text
                    style={{
                      padding: 5,
                      paddingTop: 10,
                      borderWidth: 1,
                      borderColor: '#C4C4C4',
                      borderRadius: 5,
                      // backgroundColor: 'red',
                      alignSelf: 'flex-start',
                      fontFamily: 'Poppins-Regular',
                      fontSize: 10,
                      marginRight: 10,
                    }}>
                    peralatan dapur
                  </Text>
                  <Text
                    style={{
                      padding: 5,
                      paddingTop: 10,
                      borderWidth: 1,
                      borderColor: '#C4C4C4',
                      borderRadius: 5,
                      // backgroundColor: 'red',
                      alignSelf: 'flex-start',
                      fontFamily: 'Poppins-Regular',
                      fontSize: 10,
                      marginRight: 10,
                    }}>
                    jam dinding
                  </Text>
                </View>
              </View>
            </ScrollView>
            <Gap height={20} />
            <Text style={styles.hotKeyword}>New Product</Text>
            <Gap height={10} />
            {product.map((product, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  activeOpacity={0.9}
                  onPress={() => onDetailClick(product)}>
                  <Card
                    id={product._id}
                    image={product.image}
                    name={product.name}
                    desc={product.desc}
                    price={product.price}
                    rating={product.rating}
                    onPress={() => onFavoriteClick()}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Home;

const styles = StyleSheet.create({
  page: {flexGrow: 1, backgroundColor: 'white'},
  heroContainer: {
    height: 200,
  },
  heroImage: {flex: 1, width: null, height: null, marginTop: -30},
  container: {
    paddingHorizontal: 20,
    flex: 1,
  },
  walletVoucherContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 15,
    paddingHorizontal: 15,
  },
  walletContainer: {flexDirection: 'row', alignItems: 'center'},
  walletImageContainer: {width: 20, height: 20},
  walletImage: {flex: 1, width: null, height: null},
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginTop: 30,
  },
  cashContainer: {marginLeft: 5, marginTop: 5},
  cash: {fontFamily: 'Poppins-Medium', fontSize: 10},
  searchSection: {
    // backgroundColor: 'red',
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
  },
  input: {
    padding: 8,
    paddingLeft: 10,
    height: 36,
    flex: 1,
  },
  notification: {marginLeft: 10, marginRight: 10},
  cardContainer: {marginBottom: 10},
  hotKeyword: {fontFamily: 'Poppins-Medium', fontWeight: 'bold'},
  hotKeywordContainer: {flexDirection: 'row', marginBottom: 10},
});
