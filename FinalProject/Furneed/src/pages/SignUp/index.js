import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Gap, TextInput, Button, Header} from '../../components';
import {useNavigation} from '@react-navigation/native';

const SignUp = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.page}>
      <Header title="Sign Up" onBack={navigation.goBack} />
      <View style={styles.container}>
        <TextInput label="Full Name" placeholder="Type your full name" />
        <TextInput label="Email" placeholder="Type your email" />
        <TextInput label="Password" placeholder="Type your password" />
        <TextInput label="Confirm Password" placeholder="Type your password" />
        <Button text="Sign Up" />
      </View>
    </View>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: 'white'},
  titleContainer: {marginLeft: 30},
  title: {fontFamily: 'Poppins-Medium', fontSize: 26, fontWeight: 'bold'},
  container: {paddingHorizontal: 30, flex: 1, backgroundColor: 'white'},
  create: {
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
    color: '#1F93FF',
    alignSelf: 'center',
  },
});
