import React from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import {Gap, Header, Card, Button} from '../../components';

import ImageProduct1 from '../../assets/Dummy/lidkullen.jpg';
import ImageProduct2 from '../../assets/Dummy/rakkestad.jpg';
import ImageProduct3 from '../../assets/Dummy/skarig.jpg';
import ImageProduct4 from '../../assets/Dummy/gronlid.jpg';
import {ScrollView} from 'react-native-gesture-handler';
import {BoxShadow} from 'react-native-shadow';

const Cart = () => {
  const windowWidth = Dimensions.get('window').width;

  const shadowOpt = {
    width: windowWidth,
    height: 150,
    color: '#000',
    border: 10,
    radius: 20,
    opacity: 0.1,
    x: 0,
    y: -1,
  };
  const onRemoveClick = () => {
    alert('Succes delete item');
  };
  return (
    <View style={styles.page}>
      <Header title="CART" hasDivider />
      <ScrollView>
        <View style={styles.container}>
          <Card
            image={ImageProduct2}
            name="RAKKESTAD"
            desc="Lemari pakaian 2 pintu, hitam-cokelat"
            qty={3}
            price="Rp1.799.000"
            rating="4.8"
            onPress={() => onRemoveClick()}
            productFor="FOR-CART"
          />
          <Gap height={10} />
          <Card
            image={ImageProduct4}
            name="GRÖNLID"
            desc="Sofa 2 dudukan, Ljungen merah cerah"
            qty={1}
            price="Rp7.495.000"
            rating="4.9"
            onPress={() => onRemoveClick()}
            productFor="FOR-CART"
          />
          <Gap height={10} />
          <Card
            image={ImageProduct3}
            name="SKARIG"
            desc="Sofa 2 dudukan, Ljungen merah cerah"
            qty={4}
            price="Rp199.000"
            rating="4.9"
            onPress={() => onRemoveClick()}
            productFor="FOR-CART"
          />
          <Gap height={10} />
          <Card
            image={ImageProduct4}
            name="GRÖNLID"
            desc="Sofa 2 dudukan, Ljungen merah cerah"
            qty={2}
            price="Rp7.495.000"
            rating="4.9"
            onPress={() => onRemoveClick()}
            productFor="FOR-CART"
          />
          <Gap height={10} />
          <Card
            image={ImageProduct4}
            name="GRÖNLID"
            desc="Sofa 2 dudukan, Ljungen merah cerah"
            qty={3}
            price="Rp7.495.000"
            rating="4.9"
            onPress={() => onRemoveClick()}
            productFor="FOR-CART"
          />
          <Gap height={10} />
          <Card
            image={ImageProduct4}
            name="GRÖNLID"
            desc="Sofa 2 dudukan, Ljungen merah cerah"
            qty={1}
            price="Rp7.495.000"
            rating="4.9"
            onPress={() => onRemoveClick()}
            productFor="FOR-CART"
          />
        </View>
      </ScrollView>
      <BoxShadow setting={shadowOpt}>
        <View style={styles.bottomContainer}>
          <View style={styles.totalContainer}>
            <Text style={styles.label}>Sub Total</Text>
            <Text style={styles.label}>Rp.23.560.000</Text>
          </View>
          <View style={styles.totalContainer}>
            <Text style={styles.label}>Shipping</Text>
            <Text style={styles.label}>Rp.360.000</Text>
          </View>
          <View style={styles.totalContainer}>
            <Text style={styles.totalText}>Total</Text>
            <Text style={styles.totalText}>Rp.23.560.000</Text>
          </View>
          <View style={styles.buttonContainer}>
            <Button text="Checkout" onPress={() => alert('Success checkout')} />
          </View>
        </View>
      </BoxShadow>
    </View>
  );
};

export default Cart;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: 'white'},
  container: {paddingHorizontal: 30},
  bottomContainer: {
    height: 150,
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 10,
    paddingTop: 15,
    justifyContent: 'space-between',
  },
  totalContainer: {
    // height: 50,
    marginBottom: 5,
    borderRadius: 5,
    // backgroundColor: '#F6F6F6',
    paddingHorizontal: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {fontFamily: 'Poppins-Medium', fontSize: 12, color: '#020202'},
  totalText: {
    fontFamily: 'Poppins-Medium',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#020202',
  },
  buttonContainer: {marginTop: 10},
});
