import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {set} from 'react-native-reanimated';
import {Gap, TextInput, Button, Header} from '../../components';

const SignIn = ({navigation}) => {
  const [form, setForm] = useState({email: '', password: ''});

  const onSubmit = () => {
    if (form.email == 'admin' && form.password == 'admin') {
      alert('Success login');
      setTimeout(() => {
        navigation.replace('MainApp');
      }, 800);
    } else {
      alert(`Silahkan masukan username & password "admin"`);
    }
  };

  return (
    <View style={styles.page}>
      <Header title="Sign In" />
      <View style={styles.container}>
        <TextInput
          id="email"
          label="Email"
          placeholder="Type your email address"
          onChangeText={(value) => setForm({...form, email: value})}
        />
        <TextInput
          id="password"
          label="Password"
          placeholder="Type your password"
          secureTextEntry
          onChangeText={(value) => setForm({...form, password: value})}
        />
        <Button text="Sign In" onPress={onSubmit} />
        <Gap height={20} />
        <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
          <Text style={styles.create}>Don't have an account? Sign Up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: 'white'},
  titleContainer: {marginLeft: 30},
  title: {fontFamily: 'Poppins-Medium', fontSize: 26, fontWeight: 'bold'},
  container: {paddingHorizontal: 30, flex: 1, backgroundColor: 'white'},
  create: {
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
    color: '#1F93FF',
    alignSelf: 'center',
  },
});
