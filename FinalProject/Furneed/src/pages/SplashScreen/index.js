import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Animated} from 'react-native';
import {Logo} from '../../assets';
import Fade from 'react-reveal/Fade';

const SplashScreen = ({navigation}) => {
  const [slogan, setSlogan] = useState('');
  const [opacity, setOpacity] = useState(0);
  const [opacityBrand, setOpacityBrand] = useState(0);

  const letter = 'design as you want..';
  useEffect(() => {
    let numberOpacity = 0;
    setInterval(() => {
      if (numberOpacity < 10) {
        setOpacity(numberOpacity / 10);
        setOpacityBrand(numberOpacity / 10);
        numberOpacity++;
      }
    }, 50);

    let number = 0;
    let word = slogan;
    setTimeout(() => {
      setInterval(() => {
        if (number < letter.length) {
          setSlogan(word + letter[number]);
          word = word + letter[number];
          number++;
        }
      }, 50);
    }, 1000);

    setTimeout(() => {
      navigation.replace('SignIn');
    }, 2700);
  }, []);

  return (
    <View style={styles.page}>
      <Logo opacity={opacity} />
      <Text style={styles.brand(opacityBrand)}>
        <Text style={{color: '#FCA500'}}>FUR</Text>
        <Text style={{color: '#515151'}}>NEED</Text>
      </Text>
      <Text style={styles.slogan}>{slogan}</Text>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  brand: (opacity) => ({
    fontFamily: 'Poppins-Medium',
    fontSize: 28,
    fontWeight: 'bold',
    marginTop: 20,
    opacity: opacity,
  }),
  slogan: {fontSize: 10, letterSpacing: 1, color: '#676767', marginTop: 5},
});
