import React from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import {TabView, SceneMap} from 'react-native-tab-view';
import {Gap, Header, Card, Button, OrderTabSection} from '../../components';

const Order = () => {
  return (
    <View style={styles.page}>
      <Header title="ORDER" />
      <View style={styles.container}>
        <OrderTabSection />
      </View>
    </View>
  );
};

export default Order;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: 'white'},
  container: {flex: 1, marginTop: -30},
});
