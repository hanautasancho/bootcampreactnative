import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  ImageBackground,
  Dimensions,
  StatusBar,
} from 'react-native';
import {IconStarOn, IconBack} from '../../assets';
import {Button} from '../../components';
import {BoxShadow} from 'react-native-shadow';
import {TouchableOpacity} from 'react-native-gesture-handler';

const Detail = (props) => {
  const product = props.route.params.product;
  const [counter, setCounter] = useState(1);
  const [priceFormated, setPriceFormated] = useState(0);

  const handleMinus = () => {
    if (counter > 1) {
      setCounter(counter - 1);
      setPriceFormated(formatRupiah(product.price * (counter - 1)));
    }
  };

  const handlePlus = () => {
    setCounter(counter + 1);
    setPriceFormated(formatRupiah(product.price * (counter + 1)));
  };

  useEffect(() => {
    setPriceFormated(formatRupiah(product.price));
  }, []);

  const formatRupiah = (bilangan) => {
    var number_string = bilangan.toString();
    var sisa = number_string.length % 3;
    var rupiah = number_string.substr(0, sisa);
    var ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
      var separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    return rupiah;
  };

  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const height = windowHeight - 360;
  const shadowOpt = {
    width: windowWidth,
    height: height,
    color: '#000',
    border: 15,
    radius: 30,
    opacity: 0.1,
    x: 0,
    y: -1,
    style: {marginTop: 10},
  };
  return (
    <View style={styles.page}>
      <StatusBar translucent backgroundColor="transparent" />
      <ImageBackground
        source={{
          uri: `https://api-furneed.herokuapp.com/images/${product.image}`,
        }}
        style={{height: 350}}>
        <IconBack style={styles.back} width={30} height={30} />
      </ImageBackground>
      <BoxShadow setting={shadowOpt}>
        <View style={styles.container}>
          <View style={styles.detail}>
            <View style={styles.topContainer}>
              <View>
                <Text style={styles.name}>{product.name}</Text>
                <View style={styles.ratingContainer}>
                  <IconStarOn />
                  <Text style={styles.ratingNumber}>{product.rating}</Text>
                </View>
              </View>
              <View style={styles.counterContainer}>
                <TouchableOpacity onPress={handleMinus}>
                  <View style={styles.minus}>
                    <Text>-</Text>
                  </View>
                </TouchableOpacity>
                <Text style={styles.counter}>{counter}</Text>
                <TouchableOpacity onPress={handlePlus}>
                  <View style={styles.plus}>
                    <Text>+</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <ScrollView
              style={styles.descContainer}
              showsVerticalScrollIndicator={false}>
              <Text style={styles.desc}>{product.desc}</Text>
            </ScrollView>
            <View style={styles.stockContainer}>
              <Text style={styles.stock}>Stok tersisa {product.stock}</Text>
            </View>
          </View>
          <View style={styles.bottomContainer}>
            <View style={styles.totalContainer}>
              <Text style={styles.labelTotalPrice}>Total Price:</Text>
              <Text style={styles.totalPrice}>Rp{priceFormated}</Text>
            </View>
            <View style={styles.buttonContainer}>
              <Button
                text="Add To Cart"
                onPress={() => alert('Success add to cart')}
              />
            </View>
          </View>
        </View>
      </BoxShadow>
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: 'white'},
  back: {marginTop: 10, marginLeft: 10},
  container: {
    flex: 1,
    paddingTop: 30,
    paddingBottom: 10,
    paddingHorizontal: 20,
    // borderWidth: 1,
    // borderColor: 'red',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: 'white',
    // justifyContent: 'space-between',
  },
  detail: {
    flexDirection: 'column',
    flex: 1,
  },
  topContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  name: {
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#020202',
  },
  ratingContainer: {flexDirection: 'row', alignItems: 'center'},
  ratingNumber: {fontSize: 12, marginLeft: 3},
  counterContainer: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#C4C4C4',
    width: 90,
    height: 30,
    marginLeft: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  minus: {
    width: 30,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  counter: {fontFamily: 'Poppins-Regular', alignSelf: 'center', paddingTop: 5},
  plus: {
    width: 30,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  descContainer: {
    flex: 1,
    paddingTop: 10,
    overflow: 'hidden',
  },
  desc: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#8D92A3',
  },
  stockContainer: {
    paddingBottom: 10,
  },
  stock: {
    fontFamily: 'Poppins-Medium',
    fontSize: 10,
    color: '#12A89D',
  },
  price: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FCA500',
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  totalContainer: {marginRight: 20},
  labelTotalPrice: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#8D92A3',
  },
  totalPrice: {
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    color: '#FCA500',
    fontWeight: 'bold',
  },
  buttonContainer: {flex: 1},
});
