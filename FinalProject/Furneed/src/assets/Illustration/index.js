import Logo from './Logo.svg';
import Wallet from './Wallet.png';
import Voucher from './Voucher.png';
import profileBackground from './profile-bg1.jpg';

export {Logo, Wallet, Voucher, profileBackground};
