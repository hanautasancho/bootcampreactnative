import IconBack from './icon-back.svg';
import IconSearch from './icon-search.svg';
import IconHomeOn from './icon-home-on.svg';
import IconHomeOff from './icon-home-off.svg';
import IconTimeOn from './icon-time-on.svg';
import IconTimeOff from './icon-time-off.svg';
import IconCartOn from './icon-cart-on.svg';
import IconCartOff from './icon-cart-off.svg';
import IconProfileOn from './icon-profile-on.svg';
import IconProfileOff from './icon-profile-off.svg';
import IconFavoriteOn from './icon-favorite-on.svg';
import IconFavoriteOff from './icon-favorite-off.svg';
import IconStarOn from './icon-star-on.svg';
import IconStarOff from './icon-star-off.svg';
import IconRemove from './icon-remove.svg';
import IconBellOff from './icon-bell-off.svg';
import IconChat from './icon-chat.svg';
import IconEdit from './icon-edit.svg';

export {
  IconBack,
  IconSearch,
  IconHomeOn,
  IconHomeOff,
  IconTimeOn,
  IconTimeOff,
  IconCartOn,
  IconCartOff,
  IconProfileOn,
  IconProfileOff,
  IconFavoriteOn,
  IconFavoriteOff,
  IconStarOn,
  IconStarOff,
  IconRemove,
  IconBellOff,
  IconChat,
  IconEdit,
};
