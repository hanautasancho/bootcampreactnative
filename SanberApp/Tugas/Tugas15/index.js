import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { SignIn, CreateAccount } from "./Screen";
import { SkillScreen, ProjectScreen, AddScreen, AboutScreen } from "../TugasNavigation";

const AuthStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const DrawerScreen = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={TabScreen} />
      <Drawer.Screen name="About" component={AboutScreen} />
    </Drawer.Navigator>
  );
};

const TabScreen = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        tabStyle: {
          justifyContent: "center",
        },
      }}
    >
      <Tab.Screen name="Skill" component={SkillScreen} />
      <Tab.Screen name="Project" component={ProjectScreen} />
      <Tab.Screen name="Add" component={AddScreen} />
    </Tab.Navigator>
  );
};

export default () => (
  <NavigationContainer>
    <AuthStack.Navigator>
      <AuthStack.Screen name="SignIn" component={SignIn} options={{ headerShown: false }} />
      <AuthStack.Screen name="CreateAccount" component={DrawerScreen} options={{ headerShown: false }} />
    </AuthStack.Navigator>
  </NavigationContainer>
);
