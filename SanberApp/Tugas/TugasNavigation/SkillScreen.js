import React from "react";
import { StyleSheet, Text, View } from "react-native";

const SkillScreen = () => {
  return (
    <View style={styles.page}>
      <Text>SkillScreen</Text>
    </View>
  );
};

export default SkillScreen;

const styles = StyleSheet.create({
  page: { flex: 1, justifyContent: "center", alignItems: "center" },
});
