import React from "react";
import { StyleSheet, Text, View } from "react-native";

const AddScreen = () => {
  return (
    <View style={styles.page}>
      <Text>Halaman Tambah</Text>
    </View>
  );
};

export default AddScreen;

const styles = StyleSheet.create({
  page: { flex: 1, justifyContent: "center", alignItems: "center" },
});
