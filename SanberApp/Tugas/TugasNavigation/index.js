import SkillScreen from "./SkillScreen";
import ProjectScreen from "./ProjectScreen";
import AddScreen from "./AddScreen";
import AboutScreen from "./AboutScreen";

export { SkillScreen, ProjectScreen, AddScreen, AboutScreen };
