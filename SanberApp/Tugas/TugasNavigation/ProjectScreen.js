import React from "react";
import { StyleSheet, Text, View } from "react-native";

const ProjectScreen = () => {
  return (
    <View style={styles.page}>
      <Text>Halaman Proyek</Text>
    </View>
  );
};

export default ProjectScreen;

const styles = StyleSheet.create({
  page: { flex: 1, justifyContent: "center", alignItems: "center" },
});
