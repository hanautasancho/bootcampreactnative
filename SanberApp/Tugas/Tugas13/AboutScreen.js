import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";

const AboutScreen = () => {
  return (
    <View style={styles.page}>
      <View style={styles.header}>
        <Text style={styles.title}>About</Text>
      </View>
      <View style={styles.container}>
        <View style={styles.photo}>
          <View style={styles.borderPhoto}>
            <Image source={require("./images/photo.jpeg")} style={styles.photoContainer} />
          </View>
        </View>
        <View style={styles.profile}>
          <Text style={styles.name}>Mas Dimas Satria Ananda</Text>
          <View style={styles.line}></View>
        </View>
        <View style={{ marginTop: 35 }}></View>
        <View style={styles.contact}>
          <Image source={require("./images/fb-logo.jpg")} style={styles.logo} />
          <Text>mas.d.satria</Text>
        </View>
        <View style={styles.contact}>
          <Image source={require("./images/twitter-logo.jpg")} style={styles.logo} />
          <Text>@masdimassatriaa</Text>
        </View>
        <View style={styles.contact}>
          <Image source={require("./images/instagram-logo.jpg")} style={styles.logo} />
          <Text>@masdimassatriaa</Text>
        </View>
        <View style={styles.contact}>
          <Image source={require("./images/gitlab-logo.jpg")} style={styles.logo} />
          <Text>@hanautasancho</Text>
        </View>
      </View>
    </View>
  );
};

export default AboutScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: "#E5E5E5",
  },
  header: {
    height: 100,
    paddingTop: 17,
    paddingBottom: 17,
    backgroundColor: "white",
    paddingHorizontal: 24,
    flexDirection: "row",
    alignItems: "center",
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
  },
  gap: {
    height: 15,
  },
  container: {
    marginTop: 15,
    backgroundColor: "white",
    paddingTop: 50,
    paddingHorizontal: 24,
    height: "100%",
  },
  photo: {
    alignItems: "center",
    marginTop: 0,
    marginBottom: 10,
  },
  borderPhoto: {
    borderWidth: 2,
    borderColor: "#C4C4C4",
    width: 120,
    height: 120,
    borderRadius: 120,
    justifyContent: "center",
    alignItems: "center",
  },
  photoContainer: {
    width: 100,
    height: 100,
    borderRadius: 90,
    backgroundColor: "#F0F0F0",
    padding: 30,
  },
  profile: {
    marginTop: 20,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  name: {
    fontSize: 18,
  },
  line: {
    marginTop: 5,
    borderWidth: 2,
    borderColor: "#FF5C5C",
    width: "67%",
  },
  contact: { marginTop: 15, paddingHorizontal: 20, flexDirection: "row", alignItems: "center" },
  logo: {
    width: 37,
    height: 37,
    marginRight: 10,
  },
});
