import React from "react";
import { StyleSheet, Text, View, TextInput, Button } from "react-native";

const LoginScreen = () => {
  return (
    <View style={styles.page}>
      <View style={styles.header}>
        <Text style={styles.title}>Login</Text>
      </View>
      <View style={styles.container}>
        <Text style={styles.label}>Email Address</Text>
        <TextInput style={styles.input} placeholder="Type your email address"></TextInput>
        <View style={styles.gap}></View>
        <Text style={styles.label}>Password</Text>
        <TextInput style={styles.input} placeholder="Type your password"></TextInput>
        <View style={styles.gap}></View>
        <View style={styles.gap}></View>
        <View style={styles.button}>
          <Text style={{ fontSize: 17, color: "white", fontWeight: "bold" }}>Login</Text>
        </View>
      </View>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: "#E5E5E5",
  },
  header: {
    height: 100,
    paddingTop: 17,
    paddingBottom: 17,
    backgroundColor: "white",
    paddingHorizontal: 24,
    flexDirection: "row",
    alignItems: "center",
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
  },
  gap: {
    height: 15,
  },
  container: {
    marginTop: 15,
    backgroundColor: "white",
    paddingTop: 50,
    paddingHorizontal: 24,
    height: "100%",
  },
  label: {
    fontSize: 14,
  },
  input: {
    marginTop: 5,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 10,
    height: 45,
    padding: 12,
  },
  button: {
    borderRadius: 10,
    height: 45,
    backgroundColor: "#FF5C5C",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
});
