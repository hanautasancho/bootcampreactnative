// No.1
class Animal {
  constructor(name) {
    this.name = name;
    this.legs = 4;
    this.cold_blooded = false;
  }
}
var sheep = new Animal("shaun");

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

class Ape extends Animal {
  constructor(name) {
    super();
    this.name = name;
    this.legs = 2;
  }
  yell() {
    console.log("Auooo");
  }
}

class Frog extends Animal {
  jump() {
    console.log("hop hop");
  }
}

var sungokong = new Ape("kera sakti");
console.log(sungokong.name);
console.log(sungokong.legs);
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
console.log(kodok.name);
console.log(kodok.legs);
kodok.jump(); // "hop hop"

// No.2
class Clock {
  constructor(template) {
    this.template = template;
    this.timer;
  }
  render() {
    var newClock = new Clock("h:m:s");

    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;
    var output = newClock.template.replace("h", hours).replace("m", mins).replace("s", secs);
    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(this.render, 1000);
  }
}

var clock = new Clock("h:m:s");
clock.start();
