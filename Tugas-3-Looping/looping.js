// No.1
let number = 0;
while (number <= 20) {
  if (number === 0) {
    console.log("LOOPING PERTAMA");
  } else {
    console.log(number + " - I love coding");
  }
  number += 2;
}
while (number > 0) {
  if (number === 22) {
    console.log("LOOPING KEDUA");
  } else {
    console.log(number + " - I will become a mobile developer");
  }
  number -= 2;
}

// No.2
for (i = 1; i <= 20; i++) {
  if (i % 2 == 0) {
    console.log(i + " Berkualitas");
  } else {
    if (i % 3 == 0) {
      console.log(i + " I Love Coding ");
    } else {
      console.log(i + " Santai");
    }
  }
}

// No.3
for (i = 1; i <= 4; i++) {
  let a = "";
  for (j = 1; j <= 8; j++) {
    a += "#";
  }
  console.log(a);
}

// No.4
for (i = 1; i <= 7; i++) {
  let a = "";
  for (j = 1; j <= i; j++) {
    a += "#";
  }
  console.log(a);
}

// No.5
for (i = 1; i <= 8; i++) {
  let a = "";
  for (j = 1; j <= 4; j++) {
    if (i % 2 == 1) {
      a += " ";
    }
    a += "#";
    if (i % 2 == 0) {
      a += " ";
    }
  }
  console.log(a);
}
