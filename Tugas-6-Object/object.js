// No.1
function arrayToObject(arr) {
  var now = new Date();
  var thisYear = now.getFullYear();
  let age = [];
  let people;
  if (arr.length <= 0) {
    console.log("");
  } else {
    for (let i = 0; i < arr.length; i++) {
      age[i] = thisYear - arr[i][3];
      if (arr[i][3] == null || arr[i][3] > thisYear) {
        age[i] = "Invalid Birth Year";
      }

      let people = { firstName: arr[i][0], lastName: arr[i][1], gender: arr[i][2], age: age[i] };
      console.log(arr[i][0] + " " + arr[i][1], people);
    }
  }
}

var arr = [
  ["Abduh", "Muhamad", "male", 1992],
  ["Ahmad", "Taufik", "male", 1985],
];

arrayToObject(arr);

// No.2
function shoppingTime(memberId, money) {
  let max = 0;
  let saleItemBrand = "";
  let maxSaleItem = 0;
  let listPurchased = [];
  let changeMoney = 0;
  const listItem = [
    { brand: "Sepatu Stacattu", harga: 1500000 },
    { brand: "Baju Zoro", harga: 500000 },
    { brand: "Baju H&N", harga: 250000 },
    { brand: "Casing Handphone", harga: 50000 },
    { brand: "Sweater Uniklooh", harga: 175000 },
  ];

  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }

  if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  }

  for (let i = 1; i < listItem.length; i++) {
    if (money >= listItem[i].harga) {
      if (listItem[i].harga > max) {
        listPurchased.pop();
        listPurchased.push(listItem[i].brand);
        max = listItem[i].harga;
        changeMoney = money - max;
      }
    }
    if (changeMoney >= listItem[i].harga) {
      if (listItem[i].harga > maxSaleItem) {
        saleItemBrand = listItem[i].brand;
        maxSaleItem = listItem[i].harga;
      }
    }
  }

  listPurchased.push(saleItemBrand);
  changeMoney = changeMoney - maxSaleItem;

  return { memberId: memberId, money: money, listPurchased: listPurchased, changeMoney: changeMoney };
}

let memberId = "324193hDew2";
let money = 700000;

console.log(shoppingTime(memberId, money));

// No.3
function naikAngkot(arrPenumpang) {
  if (arrPenumpang.length <= 0) {
    return [];
  } else {
    let obj = [];
    rute = ["A", "B", "C", "D", "E", "F"];
    for (let i = 0; i < arrPenumpang.length; i++) {
      var rutePenumpang = rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]);
      var bayar = rutePenumpang * 2000;
      obj[i] = {
        penumpang: arrPenumpang[i][0],
        naikDari: arrPenumpang[i][1],
        tujuan: arrPenumpang[i][2],
        bayar: bayar,
      };
    }
    return obj;
  }
}
// console.log(naikAngkot([]));
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
