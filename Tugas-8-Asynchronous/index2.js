var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

let time = 10000;
let index = 0;

const cb = (remainingTime, index) => {
  if (index < books.length) {
    readBooksPromise(remainingTime, books[index]).then((res) => cb(res, index + 1));
  }
};
cb(time, index);
