// No.1
function range(startNum, finishNum) {
  let arr = [];
  if (startNum && finishNum) {
    if (startNum > finishNum) {
      [startNum, finishNum] = [finishNum, startNum];
      for (let i = startNum; i <= finishNum; i++) {
        arr.push(i);
      }
      arr = arr.sort(function (value1, value2) {
        return value2 - value1;
      });
    } else {
      for (let i = startNum; i <= finishNum; i++) {
        arr.push(i);
      }
    }
    return arr;
  } else {
    return -1;
  }
}
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

// No.2
function rangeWithStep(startNum, finishNum, step) {
  if (startNum && finishNum) {
    let arr = [];
    if (startNum > finishNum) {
      [startNum, finishNum] = [finishNum, startNum];
      for (let i = startNum; i <= finishNum; i = i + step) {
        arr.push(i);
      }
      arr = arr.sort(function (value1, value2) {
        return value2 - value1;
      });
    } else {
      for (let i = startNum; i <= finishNum; i = i + step) {
        arr.push(i);
      }
    }

    return arr;
  } else {
    return -1;
  }
}
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

// No.3
function sum(startNum, finishNum, step) {
  if (startNum || finishNum) {
    let arr = [];
    finishNum = finishNum ? finishNum : startNum;
    let total = 0;
    step = step ? step : 1;
    if (startNum > finishNum) {
      for (let i = startNum; i >= finishNum; i = i - step) {
        arr.push(i);
        total += i;
      }
    } else {
      for (let i = startNum; i <= finishNum; i = i + step) {
        arr.push(i);
        total += i;
      }
    }
    return total;
  } else {
    return 0;
  }
}
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

// No.4
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

function dataHandling(n) {
  var res = ``;
  for (let i = 0; i < n.length; i++) {
    res += `
    Nomor ID:  ${n[i][0]}
    Nama Lengkap:  ${n[i][1]}
    TTL:  ${n[i][2]}
    Hobi:  ${n[i][3]}
    `;
  }
  return res;
}
console.log(dataHandling(input));

// No.5
function balikKata(param) {
  let reverseWord = ``;
  for (let i = param.length; i > 0; i--) {
    reverseWord += `${param[i - 1]}`;
  }
  return reverseWord;
}
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

// No.6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
input.splice(4, 1, "Pria", "SMA Internasional Metro");
console.log(input);

let splitBulan = input[3].split("/");
switch (splitBulan[1]) {
  case "01": {
    bulan = "Januari";
    break;
  }
  case "02": {
    bulan = "Februari";
    break;
  }
  case "03": {
    bulan = "Maret";
    break;
  }
  case "04": {
    bulan = "April";
    break;
  }
  case "05": {
    bulan = "Mei";
    break;
  }
  case "06": {
    bulan = "Juni";
    break;
  }
  case "07": {
    bulan = "Juli";
    break;
  }
  case "08": {
    bulan = "Agustus";
    break;
  }
  case "09": {
    bulan = "September";
    break;
  }
  case "10": {
    bulan = "Oktober";
    break;
  }
  case "11": {
    bulan = "November";
    break;
  }
  case "12": {
    bulan = "Desember";
    break;
  }
}
console.log(bulan);
let desc = [...splitBulan];
desc.sort(function (value1, value2) {
  return value2 - value1;
});
console.log(desc);

console.log(splitBulan.join("-"));
console.log(input[1].slice(0, 15));
